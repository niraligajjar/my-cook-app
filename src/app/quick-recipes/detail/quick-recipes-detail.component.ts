import { Component } from '@angular/core';
import { StateService } from '@uirouter/angular';

import { QuickRecipesDetailService } from './quick-recipes-detail.service';


@Component({
    selector: 'quick-recipes-detail',
    templateUrl: './quick-recipes-detail.component.html',
    styleUrls: ['./quick-recipes-detail.component.css'],
    providers: [ QuickRecipesDetailService ]
})


export class QuickRecipeDetailComponent {
	recepies: any;

	constructor(
		public quickRecipesService : QuickRecipesDetailService,
		private $state: StateService
	) {}

	ngOnInit() {
		//this.recepies = this.quickRecipesService.quickRecipesDetail();
	}

	getDetailRecepie() {
		this.$state.go('dashboard.quick.detail');
	}
}