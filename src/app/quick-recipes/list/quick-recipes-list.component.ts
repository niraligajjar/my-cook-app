import { Component } from '@angular/core';
import { StateService } from '@uirouter/angular';

import { QuickRecipesListService } from './quick-recipes-list.service';


@Component({
    selector: 'quick-recipes-list',
    templateUrl: './quick-recipes-list.component.html',
    styleUrls: ['./quick-recipes-list.component.css'],
    providers: [ QuickRecipesListService ]
})


export class QuickRecipesListComponent {
	recepies: any;

	constructor(
		public quickRecipesListService : QuickRecipesListService,
		private $state: StateService
	) {}

	ngOnInit() {
		this.recepies = this.quickRecipesListService.quickRecipesList();
	}

	getDetailRecepie() {
		this.$state.go('dashboard.quick.detail');
	}
}