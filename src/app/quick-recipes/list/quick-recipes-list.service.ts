import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class QuickRecipesListService {
    quickRecipes: any;

    constructor() {
        this.quickRecipes = [];
    }

    quickRecipesList() {
        return {
            data: [
                {
                    id: 1,
                    name: 'Creamy Garlic Pasta with Shrimp & Vegetables',
                    image: 'sdsdsd',
                    description: 'Toss a garlicky, Middle Eastern-inspired yogurt sauce with pasta, shrimp, asparagus, peas and red bell pepper for a fresh, satisfying summer meal. Serve with: Slices of cucumber and tomato tossed with lemon juice and olive oil.'
                },{
                    id: 2,
                    name: 'Chicken, potato and spinach soup',
                    image: 'sdsdsd',
                    description: 'Heat the oil in a large saucepan over medium heat. Add the onion and cook, stirring, for 5 minutes or until soft. Add the garlic and cook, stirring, for 1 minute or until aromatic.'
                },{
                    id: 3,
                    name: 'Baked Sweet Potatoes',
                    image: '',
                    description: 'Preheat oven to 350 degrees F (175 degrees C). Coat the bottom of a glass or non-stick baking dish with olive oil, just enough to coat.'
                },{
                    id: 4,
                    name: 'Peanut Sesame Noodles and Veggies Recipe',
                    image: 'sdsdsd',
                    description: 'These noodles are ready in less time than it takes to order takeout! So next time you’re in a rush, give these peanut sesame noodles a try. '
                },{
                    id: 5,
                    name: 'Breakfast Smoothie',
                    image: 'sdsdsd',
                    description: 'Everyone knows that breakfast is the most important meal of the day, but that does not make preparing and eating it any easier.'
                },{
                    id: 6,
                    name: 'Creamy Garlic Pasta with Shrimp & Vegetables',
                    image: 'sdsdsd',
                    description: 'Toss a garlicky, Middle Eastern-inspired yogurt sauce with pasta, shrimp, asparagus, peas and red bell pepper for a fresh, satisfying summer meal. Serve with: Slices of cucumber and tomato tossed with lemon juice and olive oil.'
                },{
                    id: 7,
                    name: 'Chicken, potato and spinach soup',
                    image: 'sdsdsd',
                    description: 'Heat the oil in a large saucepan over medium heat. Add the onion and cook, stirring, for 5 minutes or until soft. Add the garlic and cook, stirring, for 1 minute or until aromatic.'
                },{
                    id: 8,
                    name: 'Baked Sweet Potatoes',
                    image: '',
                    description: 'Preheat oven to 350 degrees F (175 degrees C). Coat the bottom of a glass or non-stick baking dish with olive oil, just enough to coat.'
                },{
                    id: 9,
                    name: 'Peanut Sesame Noodles and Veggies Recipe',
                    image: 'sdsdsd',
                    description: 'These noodles are ready in less time than it takes to order takeout! So next time you’re in a rush, give these peanut sesame noodles a try. '
                },{
                    id: 10,
                    name: 'Breakfast Smoothie',
                    image: 'sdsdsd',
                    description: 'Everyone knows that breakfast is the most important meal of the day, but that does not make preparing and eating it any easier.'
                }
            ]
        };
    }
}