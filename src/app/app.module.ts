import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { UIRouterModule, UIRouter } from "@uirouter/angular";
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';

import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LunchRecipesComponent } from './lunch-recipes/lunch-recipes.component';
import { BreakfastRecipesComponent } from './breakfast-recipes/breakfast-recipes.component';
import { QuickRecipesListComponent } from './quick-recipes/list/quick-recipes-list.component';
import { Child } from './broadcast-event/broadcast-event-child.component';
import { BroadcastEventComponent } from './broadcast-event/broadcast-event.component';
import { DinnerRecipesComponent} from './dinner-recipes/dinner-recipes.component';
import { AppPromiseService } from './services/app.service';
import { Broadcaster } from './services/broadcast.service';
import { QuickRecipeDetailComponent} from './quick-recipes/detail/quick-recipes-detail.component'

var statesAry = [
    {name: 'login', url: '/login',  component: LoginComponent},
    {name: 'registration', url: '/registration', component: RegistrationComponent },
    {name: 'dashboard', url: '/dashboard', component: DashboardComponent },
    {name: 'dashboard.broadcast', url: '/broadcast', component: BroadcastEventComponent },
    {name: 'dashboard.quick', url: '/quick', component: QuickRecipesListComponent },
    {name: 'dashboard.quick.detail', url: '/detail', component: QuickRecipeDetailComponent },
    {name: 'dashboard.breakFast', url: '/breakfast', component: BreakfastRecipesComponent },
    {name: 'dashboard.lunch', url: '/lunch', component: LunchRecipesComponent },
    {name: 'dashboard.dinner', url: '/dinner', component: DinnerRecipesComponent }
];

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        RegistrationComponent,
        DashboardComponent,
        QuickRecipesListComponent,
        BreakfastRecipesComponent,
        LunchRecipesComponent,
        Child,
        DinnerRecipesComponent,
        BroadcastEventComponent,
        QuickRecipeDetailComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,  
        ReactiveFormsModule,      
        UIRouterModule.forRoot({ states: statesAry, useHash: true, config: uiRouterConfigFn })
    ],
    providers: [ AppPromiseService, Broadcaster ],
    bootstrap: [ AppComponent]
})
export class AppModule { }

export function uiRouterConfigFn(router: UIRouter) {
    router.urlService.rules.otherwise({ state: 'login' });
}
