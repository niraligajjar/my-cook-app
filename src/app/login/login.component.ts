import { Component } from '@angular/core';
import { StateService } from '@uirouter/angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Login } from './login';
import { LoginService } from './login.service';

@Component({
    selector: 'login',
    templateUrl: './login.html',
    styleUrls: ['./login.css'],
    providers: [ LoginService ]
})


export class LoginComponent {
	model = new Login('', '');

	data = {username: "abc", password: "abc"}
	submitted = false;
	heroForm: FormGroup;

	constructor(
		public loginService: LoginService,
		private $state: StateService
	) {
		console.log(this.data)
	}

	ngOnInit(): void {
	  	this.heroForm = new FormGroup({
	    	'username': new FormControl(this.data.username, [
	      		Validators.required,
	      		Validators.minLength(4)
	    	]),
	    	'password': new FormControl(this.data.password, Validators.required)
	  	});
	}


	login() {
		this.submitted = true;
		var postObj = {
			'userName': this.model.username,
			'passWord': this.model.password
		};
		var temp = this.loginService.login(postObj);
		if (temp == true) {
			this.$state.go('dashboard');
			window.localStorage.setItem('currentUser', JSON.stringify(postObj));
		} else {
			alert("wrong credentials");
		}
	}
}