import { Component, Input } from '@angular/core';
import { Broadcaster } from '../services/broadcast.service';
import { AppPromiseService } from '../services/app.service';

@Component({
    selector: 'child',
    template: `
        <div class="container">
            <b>Child#{{childID}}</b>
            <button (click)="emit()">Emit</button>
            <span>{{message}}</span>
            <ng-content></ng-content>
        </div>
    `,
    styles: [`
        .container {
            border: solid 1px black;
            padding: 5px;
            margin: 5px;
            width: 99%;
        }
    `],
    providers: [ AppPromiseService ],
})
export class Child {
    @Input() childID: string;
    message: string;
    _timer: any;

    constructor(
        private broadcaster: Broadcaster,
        private appPromiseService: AppPromiseService
    ) {}

    ngOnInit() {
        this.registerTypeBroadcast();
    }

    registerStringBroadcast() {
        this.broadcaster.on<string>('message')
        .subscribe(message => {
            this.message = message;
            if (this._timer) {
                clearTimeout(this._timer);
            }
            this._timer = setTimeout(() => {
                this.message = '';
                this._timer = null;
            }, 3000);
        });
    }

    registerTypeBroadcast() {
        this.appPromiseService.on()
        .subscribe(message => {
            this.message = message;
            if (this._timer) {
                clearTimeout(this._timer);
            }
            this._timer = setTimeout(() => {
                this.message = '';
                this._timer = null;
            }, 3000);
        });
    }

    emit() {
        this.emitTypeBroadcast();
    }

    emitStringBroadcast() {
        this.broadcaster.broadcast('message', `Message from ${this.childID}`);
    }

    emitTypeBroadcast() {
        this.appPromiseService.fire(`Message from ${this.childID}`);
    }
}
