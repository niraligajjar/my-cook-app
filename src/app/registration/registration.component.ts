import { Component } from '@angular/core';
import { StateService } from '@uirouter/angular';

import { Registration } from './registration';
import { RegistrationService } from './registration.service'


@Component({
    selector: 'registration',
    templateUrl: './registration.html',
    styleUrls: ['./registration.css'],
    providers: [ RegistrationService ]
})


export class RegistrationComponent {

	model = new Registration('','', '', '', '');

	constructor(
		public registrationService: RegistrationService,
		private $state: StateService
	) {}

	register () {
		var postObj = {
			'firstName': this.model.firstname,
			'lastName': this.model.lastname,
			'email': this.model.email,
			'userName': this.model.username,
			'passWord': this.model.password
		}
		if(this.model.firstname && this.model.lastname && this.model.email && this.model.username && this.model.password) {
			var temp = this.registrationService.register(postObj);
			if (temp == true) {
				this.$state.go('dashboard.menu');
				window.localStorage.setItem('currentUser', JSON.stringify(postObj));
			} else {
				alert("wrong credentials");
			}
		}
	}

}