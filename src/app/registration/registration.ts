export class Registration {

	constructor(
	    public firstname: string,
	    public lastname	: string,
	    public email	: string,
	    public username	: string,
	    public password	: string
  	) {  }

}