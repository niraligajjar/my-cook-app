import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AppPromiseService } from '../services/app.service';

@Component({
    selector: 'dashboard',
    templateUrl: './dashboard.html',
    styleUrls: ['./dashboard.css']
})


export class DashboardComponent {
	
	constructor(public appPromiseService: AppPromiseService) {}

	ngOnInit() {
        this.appPromiseService.doAsyncTask().then(
            (val) => console.log(val),
            (err) => console.error(err)
        );
    }

    openNav () {
    	document.getElementById("mySidenav").style.width = "250px";
    }

    closeNav () {
    	document.getElementById("mySidenav").style.width = "0";	
    }
}