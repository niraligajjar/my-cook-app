import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import { Broadcaster } from './broadcast.service';

@Injectable()
export class AppPromiseService {
    error = false;

    constructor(private broadcaster: Broadcaster) {}

    doAsyncTask() {
        return new Promise((resolve, reject) => {
            setTimeout((error) => {
                if (error) {
                    reject('error');
                } else {
                    resolve('done');
                }
            }, 1000);
        });
    }

    fire(data: string): void {
        this.broadcaster.broadcast(MessageEvent, data);
    }

    on(): Observable<string> {
        return this.broadcaster.on<string>(MessageEvent);
    }
}